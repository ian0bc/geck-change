﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void acercaDeNosotrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Acerca Adn = new Acerca();
            Adn.Show();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Base Datos = new Base();
                Datos.conectar();
                Datos.cerrarBase();
            }
            catch(Exception Ex){
                MessageBox.Show("No se encontró el archivo con los datos de entrada, verifique"
                + " que se encuentre en la carpeta correspondiente e intentelo de nuevo.","Error",MessageBoxButtons.OK);
                this.Close();
            }

            try
            {
                Bitacora bitacora = new Bitacora();
                bitacora.conectar();
                bitacora.cerrarConexion();
            }
            catch (Exception Ex)
            {
                MessageBox.Show("No se encontró el archivo de la bitácora, verifique"
                + " que se encuentre en la carpeta correspondiente e intentelo de nuevo.", "Error", MessageBoxButtons.OK);
                this.Close();
            }
        }

        private void cambiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cambiar camb = new Cambiar();
            camb.Show();
           
        }

        private void administrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login log = new Login();
            log.Show();
        }

        private void ayudaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ayuda Help = new Ayuda();
            Help.Show();
        }
    }
}
