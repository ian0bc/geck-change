﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            User log = new User(textBox1.Text, textBox2.Text);
            if (log.get_Estado() == "Ok"){
                Admin ad = new Admin();
                ad.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show(log.get_Estado(),"Error",MessageBoxButtons.OK);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Solo personal autorizado. \nIngrese el Usuario del administrador seguido de la contraseña única.");
        }
    }
}
