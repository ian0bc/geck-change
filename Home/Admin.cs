﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb; //para conectar con Excel

namespace Home
{
    public partial class Admin : Form
    {
        public Admin()
        {
            InitializeComponent();
        }


        //Archivos
        Base Datos = new Base();
        Bitacora bitacora = new Bitacora();
        DateTime Fecha = DateTime.Now;
        //Proceso
        Cambio asd = new Cambio();
        int valor;
        

        private void Admin_Load(object sender, EventArgs e)
        {
            
            Datos.conectar();
            recargar();
             
        }

        private void radioButton10_CheckedChanged(object sender, EventArgs e)
        {
            valor = 1;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bitacora.conectar();
            bitacora.reset_dt();
            bitacora.TomarDatos(Fecha.ToShortDateString());
            dataGridView1.ReadOnly = true;
            dataGridView1.DataSource = bitacora.get_DataTable();

            bitacora.cerrarConexion();

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Login log = new Login();
            log.Show();
            Datos.cerrarBase();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (comboBox1.SelectedIndex == 0)
                {
                    asd.añadirInv(valor, Convert.ToInt32(textBox1.Text));
                    bitacora.conectar();
                    bitacora.AgregarOp(DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "Admin", "Agregó "
                        + textBox1.Text + " unidad(es) de " + valor);
                    bitacora.cerrarConexion();
                    MessageBox.Show("Elemento agregado satisfactoriamente.");
                }
                else if (comboBox1.SelectedIndex == 1)
                {
                    asd.quitarInv(valor, Convert.ToInt32(textBox1.Text));
                    bitacora.conectar();
                    bitacora.AgregarOp(DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "Admin", "Retiró "
                        + textBox1.Text + " unidad(es) de " + valor);
                    bitacora.cerrarConexion();
                    MessageBox.Show("Elemento retirado satisfactoriamente.");
                }
                else{
                    MessageBox.Show("Ustéd no seleccionó ninguna opción.","¡Atención!",MessageBoxButtons.OK);
                }
                Datos.modificarBase(asd.get_Inv(), asd.get_Montos());
                recargar();
            }
            catch(Exception EX){
                MessageBox.Show("Lo sentimos, ocurrió el siguiente error:\n" + EX,"Error",MessageBoxButtons.OK);
            }
            
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            valor = 1000;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            valor = 500;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            valor = 200;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            valor = 100;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            valor = 50;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            valor = 20;
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            valor = 10;
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            valor = 5;
        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            valor = 2;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //Función
        private void recargar()
        {
            Datos.reset_ds();
            Datos.consultarBase();

            //Label de Inventario
            label3.Text = "[ " + Datos.get_Row(0) + " ]";
            asd.set_Inv(0,Convert.ToInt32(Datos.get_Row(0)));
            label4.Text = "[ " + Datos.get_Row(1) + " ]";
            asd.set_Inv(1,Convert.ToInt32(Datos.get_Row(1)));
            label5.Text = "[ " + Datos.get_Row(2) + " ]";
            asd.set_Inv(2, Convert.ToInt32(Datos.get_Row(2)));
            label6.Text = "[ " + Datos.get_Row(3) + " ]";
            asd.set_Inv(3, Convert.ToInt32(Datos.get_Row(3)));
            label7.Text = "[ " + Datos.get_Row(4) + " ]";
            asd.set_Inv(4, Convert.ToInt32(Datos.get_Row(4)));
            label8.Text = "[ " + Datos.get_Row(5) + " ]";
            asd.set_Inv(5, Convert.ToInt32(Datos.get_Row(5)));
            label9.Text = "[ " + Datos.get_Row(6) + " ]";
            asd.set_Inv(6, Convert.ToInt32(Datos.get_Row(6)));
            label10.Text = "[ " + Datos.get_Row(7) + " ]";
            asd.set_Inv(7, Convert.ToInt32(Datos.get_Row(7)));
            label11.Text = "[ " + Datos.get_Row(8) + " ]";
            asd.set_Inv(8, Convert.ToInt32(Datos.get_Row(8)));
            label12.Text = "[ " + Datos.get_Row(9) + " ]";
            asd.set_Inv(9, Convert.ToInt32(Datos.get_Row(9)));

          
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
                return;
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Fecha = dateTimePicker1.Value.Date;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Para modificar:\n" 
                + " Seleccione una denominación seguido de una opción (Agregar o Retirar)"
                + " e ingrese la cantidad (solo números) y accione el botón \"Aceptar\". Si la cantidad a retirar es mayor que la disponible, se quitarán todas las disponibles."
                + "Para la bitácora:\n"
                + " Seleccione la fecha del día cuyas operaciones quiere verificar seguido de accionar el botón \"Bitacora\"."
                + " Si no se realizó ninguna operación ese día la tabla estará vacía.","Ayuda",MessageBoxButtons.OK);
        }
    }
}
