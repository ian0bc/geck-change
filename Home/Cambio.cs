﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home
{
    class Cambio
    {
        //Atributos
        private int Entrada;          
        private int[] Inv = new int[] {0, 2, 0, 3, 6, 5, 4, 3, 0, 0};
        private int[] Montos = new int[] { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1 };
        private string Billetes = "";
        private int cambio=0;
        private int sincambio;

        //Metodo
        public void set_Entrada(int X)
        {
            this.Entrada = X;
        }

        public string get_Billetes()
        {
            return this.Billetes;
        }
        public int[] get_Inv()
        {
            return this.Inv;
        }
        public int[] get_Montos()
        {
            return this.Montos;
        }

        //Funciones
        public void validar(){
            hayCambio();
            if (sincambio < Entrada)
            {
                Billetes = "Lo siento, Por el momento no contamos con billetes suficientes para devolverte el cambio";
            }

            else {

                for (int i = 0; i < 10; i++)//recorrer el arreglo
                {
                    if (Montos[i] >= Entrada)
                    { //valida el "billete"

                    }
                    else
                    {

                        while (Inv[i] > 0 && Montos[i] <= Entrada - cambio)
                        {
                            Inv[i] -= 1;
                            this.cambio += Montos[i];
                            this.Billetes = this.Billetes + "Unidad de "+ Montos[i].ToString() + "\n";
                        }

                    }
                }

                for (int i = 0; i < 10; i++)
                {
                    if (Montos[i] == Entrada)
                    {
                        Inv[i] += 1;
                    }
                }
            }

        }

        public void Limpiar()
        {
            cambio = 0;
            Entrada = 0;
            Billetes = "";
        }

        //Verifica tener el cambio necesario para la operación
        public void hayCambio()
        {
            sincambio = 0;
            for (int i = 0; i < 10; i++ )
            {
                if (Montos[i] < Entrada)
                {
                    sincambio += Montos[i] * Inv[i];
                }
            }
        }

        public void set_Inv(int posicion, int valor){
            this.Inv[posicion] = valor;
        }


        //Funciones de Administrador
        public void añadirInv(int Val, int cantidad)
        {
            for (int i = 0; i < 10; i++)
            {
                if(Montos[i] == Val)
                {
                    for (int j = 0; j < cantidad && Inv[i] < 50; j++)
                    {
                        Inv[i] += 1;
                    }
                }
            }
        }

        public void quitarInv(int Val, int cantidad)
        {
            for (int i = 0; i < 10; i++)
            {
                if (Montos[i] == Val)
                {
                    for (int j = 0; j < cantidad && Inv[i] > 0; j++)
                    {
                        Inv[i] -= 1;
                    }
                }
            }
        }

       

    }
}
