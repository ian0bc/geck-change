﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home
{
    public partial class Cambiar : Form
    {
        public Cambiar()
        {
            InitializeComponent();
        }

        int Valor = 0;
        Cambio asd = new Cambio();
        Bitacora bitacora = new Bitacora();
        Base Datos = new Base();

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Datos.modificarBase(asd.get_Inv(), asd.get_Montos());
            
            MessageBox.Show("Gracias por utilizar nuestro software", "Geck Tiene un mensaje para ti", MessageBoxButtons.OK);
            Datos.cerrarBase();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult resultado = MessageBox.Show("¿La denominación seleccionada es la correcta?", "Mensaje de Confirmación", MessageBoxButtons.YesNo);

                if (resultado == DialogResult.Yes)
                {
                    asd.Limpiar();
                    asd.set_Entrada(Valor);
                    asd.validar();
                    richTextBox1.Text = asd.get_Billetes();
                    bitacora.conectar();
                    bitacora.AgregarOp(DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), "Usuario", "Ingresó " + Valor + ", Recibió: \n" + asd.get_Billetes());
                    bitacora.cerrarConexion();
                }
            }
            catch(Exception Ex){
                MessageBox.Show("Lo siento, hubo un error:\n" + Ex,"Error",MessageBoxButtons.OK);
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 1000;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 500;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 200;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 100;
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 50;
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 20;
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 10;
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 5;
        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            Valor = 2;
        }

        private void Cambiar_Load(object sender, EventArgs e)
        {
            Datos.conectar();

            //añade los datos del archivo a la clase
            for (int i = 0; i < 10; i++)
            {
                asd.set_Inv(i, Convert.ToInt32(Datos.get_Row(i)));
            }
        }

    }
}
