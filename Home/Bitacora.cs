﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Data;

namespace Home
{
    class Bitacora{

        static string conexion = "Provider = Microsoft.ACE.OleDb.12.0;Data Source ="
                + " ./Bitacora.xlsx;Extended Properties = \"Excel 12.0;\"";
        private OleDbConnection conector = new OleDbConnection(conexion);
        private OleDbCommand consulta = default(OleDbCommand);
        private OleDbDataAdapter adaptador = new OleDbDataAdapter();
        private DataTable dt = new DataTable();


        public Bitacora()
        {
            //conectar();
        }

        public void conectar()
        {
            conector.Open();
        }

        public void cerrarConexion()
        {
            conector.Close();
        }
        public void TomarDatos(string Fecha)
        {
            consulta = new OleDbCommand("SELECT * FROM [Hoja1$]  WHERE Fecha=@Fecha", conector);
            consulta.Parameters.AddWithValue("@Fecha",Fecha);
            consulta.ExecuteNonQuery();
            adaptador = new OleDbDataAdapter(consulta);
            adaptador.Fill(dt);
            
        }

        public void AgregarOp(string fecha, string hora, string tipo, string Operacion)
        {
            consulta = new OleDbCommand("INSERT INTO [Hoja1$] (Fecha,Hora,Tipo,Movimiento) VALUES(@Fecha,@Hora,@Tipo,@Movimiento)",conector);
            consulta.Parameters.AddWithValue("@Fecha", fecha);
            consulta.Parameters.AddWithValue("@Hora", hora);
            consulta.Parameters.AddWithValue("@Tipo", tipo);
            consulta.Parameters.AddWithValue("@Movimiento", Operacion);
            consulta.ExecuteNonQuery();
        }

        public DataTable get_DataTable()
        {
            return this.dt;
        }

        public void reset_dt()
        {
            this.dt.Clear();
        }
    }
}
