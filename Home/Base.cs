﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home
{
    class Base
    {
       
        static string conexion = "Provider = Microsoft.ACE.OleDb.12.0;Data Source ="
            + " ./Datos.xlsx;Extended Properties = \"Excel 12.0;\"";
        private OleDbConnection conector = new OleDbConnection(conexion);
        private OleDbCommand consulta = default(OleDbCommand);
        private OleDbDataAdapter adaptador = new OleDbDataAdapter();
        private DataSet ds = new DataSet();

        public Base()
        {
            consulta = new OleDbCommand("SELECT * FROM [Hoja1$]", conector);
            consultarBase();
        }

        public void conectar()
        {
            conector.Open();
        }

        public void cerrarBase()
        {
            conector.Close();
        }

        public void consultarBase()
        {
            consulta = new OleDbCommand("SELECT * FROM [Hoja1$]", conector);
            adaptador.SelectCommand = consulta;
            adaptador.Fill(ds);
        }

        public DataSet get_DataSet()
        {
            return this.ds;
        }

        public void reset_ds()
        {
            this.ds.Clear();
        }

        public string get_Row(int Fila){
            DataRow row = ds.Tables[0].Rows[Fila];
            string cell = row[1].ToString();
            return cell;
        }

        //Modificar archivo
        public void modificarBase(int[] cant, int[] Denominacion)
        {
            for (int i = 0; i < 10; i++)
            {
                consulta = new OleDbCommand("UPDATE [Hoja1$] SET Cantidad=@Cantidad WHERE Denominación=@Denominación ", conector);
                consulta.Parameters.AddWithValue("@Cantidad",cant[i]);
                consulta.Parameters.AddWithValue("@Denominación",Denominacion[i]);
                consulta.ExecuteNonQuery();
            }
        }
    }
}
